module Main exposing (Flags, Model, Msg(..), init, main, subscriptions, update, view)

-- Imports ---------------------------------------------------------------------

import Angle exposing (Angle)
import Browser
import Browser.Events exposing (onKeyPress)
import Json.Decode as Decode
import Svg.Styled exposing (Svg, circle, g, line, svg, text, text_)
import Svg.Styled.Attributes exposing (..)
import Svg.Styled.Events exposing (onClick)



-- Main ------------------------------------------------------------------------


main : Program Flags Model Msg
main =
    Browser.element
        { init = init
        , update = update
        , view = view >> Svg.Styled.toUnstyled
        , subscriptions = subscriptions
        }



-- Model -----------------------------------------------------------------------


type alias Flags =
    ()


type alias Model =
    { angle : Angle
    , highlight : Maybe Char
    }


init : Flags -> ( Model, Cmd Msg )
init _ =
    ( { angle = Angle.turns 0
      , highlight = Nothing
      }
    , Cmd.none
    )



-- Update ----------------------------------------------------------------------


type Msg
    = IncreaseAngle
    | DecreaseAngle
    | CharacterKey Char
    | ControlKey String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        IncreaseAngle ->
            ( { model
                | angle =
                    (Angle.inTurns model.angle + 1 / 26)
                        |> Angle.turns
              }
            , Cmd.none
            )

        DecreaseAngle ->
            ( { model
                | angle =
                    (Angle.inTurns model.angle - 1 / 26)
                        |> Angle.turns
              }
            , Cmd.none
            )

        CharacterKey char ->
            if Char.isAlpha char then
                ( { model | highlight = Just char }, Cmd.none )

            else
                ( model, Cmd.none )

        ControlKey _ ->
            ( model, Cmd.none )



-- View ------------------------------------------------------------------------


outerRadius =
    180.0


middleRadius =
    140.0


innerRadius =
    100.0


xCenter =
    200.0


yCenter =
    200.0


view model =
    svg
        [ viewBox "0 0 400 400"
        , width "100%"
        , height "100%"
        , fontSize "1.5em"
        ]
    <|
        viewClearTextDisk model.angle
            ++ viewCipherDisk
            ++ viewRotationButtons


viewRotationButtons : List (Svg Msg)
viewRotationButtons =
    [ text_
        [ x <| String.fromFloat (xCenter - 30)
        , y <| String.fromFloat yCenter
        , textAnchor "middle"
        , alignmentBaseline "middle"
        , style "font-size: 3em; cursor: pointer; user-select: none;"
        , onClick DecreaseAngle
        ]
        [ text "-" ]
    , text_
        [ x <| String.fromFloat (xCenter + 30)
        , y <| String.fromFloat yCenter
        , textAnchor "middle"
        , alignmentBaseline "central"
        , style "font-size: 3em; cursor: pointer; user-select: none;"
        , onClick IncreaseAngle
        ]
        [ text "+" ]
    ]


viewOuterDisk : String -> Svg msg
viewOuterDisk color =
    circle
        [ cx <| String.fromFloat xCenter
        , cy <| String.fromFloat yCenter
        , r <| String.fromFloat outerRadius
        , fill color
        , stroke "black"
        , strokeWidth "2"
        , class "outer-circle"
        ]
        []


viewMiddleDisk : String -> String -> Svg msg
viewMiddleDisk fillColor strokeColor =
    circle
        [ cx <| String.fromFloat xCenter
        , cy <| String.fromFloat yCenter
        , r <| String.fromFloat middleRadius
        , fill fillColor
        , stroke strokeColor
        , strokeWidth "2"
        , class "middle-circle"
        ]
        []


viewInnerDisk : String -> Svg msg
viewInnerDisk color =
    circle
        [ cx <| String.fromFloat xCenter
        , cy <| String.fromFloat yCenter
        , r <| String.fromFloat innerRadius
        , fill color
        , stroke "black"
        , strokeWidth "2"
        , class "inner-circle"
        ]
        []


viewCipherDisk =
    [ viewMiddleDisk "black" "black"
    , viewInnerDisk "white"
    ]
        ++ viewLetters innerRadius String.toUpper "white"
        ++ viewSeparators innerRadius "lightgray"


viewSeparators : Float -> String -> List (Svg msg)
viewSeparators startingRadius color =
    List.range 65 90
        |> List.indexedMap Tuple.pair
        |> List.map Tuple.first
        |> List.map toFloat
        |> List.map (\idx -> Angle.turns (idx / 26))
        |> List.map (viewSeparator startingRadius color)


viewSeparator : Float -> String -> Angle -> Svg msg
viewSeparator startingRadius color angle =
    let
        endingRadius =
            startingRadius + 40

        startX =
            xCenter + startingRadius * cos (Angle.inRadians angle)

        startY =
            xCenter + startingRadius * sin (Angle.inRadians angle)

        endX =
            xCenter + endingRadius * cos (Angle.inRadians angle)

        endY =
            xCenter + endingRadius * sin (Angle.inRadians angle)
    in
    line
        [ x1 <| String.fromFloat startX
        , y1 <| String.fromFloat startY
        , x2 <| String.fromFloat endX
        , y2 <| String.fromFloat endY
        , stroke color
        , strokeWidth "2"
        ]
        []


viewClearTextDisk : Angle -> List (Svg msg)
viewClearTextDisk angle =
    List.singleton <|
        g
            [ style "transition: 0.3s ease-out;"
            , rotationToTransformProperty angle ( xCenter, yCenter )
            ]
        <|
            [ viewOuterDisk "lightgrey"
            , viewMiddleDisk "white" "black"
            ]
                ++ viewLetters middleRadius String.toUpper "black"
                ++ viewSeparators middleRadius "black"


viewLetters : Float -> (String -> String) -> String -> List (Svg msg)
viewLetters radius transformation color =
    List.range 65 90
        |> List.indexedMap Tuple.pair
        |> List.map (Tuple.mapSecond Char.fromCode)
        |> List.map (Tuple.mapSecond String.fromChar)
        |> List.map (Tuple.mapSecond transformation)
        |> List.map (viewLetter (radius + 20) color)


viewLetter : Float -> String -> ( Int, String ) -> Svg msg
viewLetter radius color ( intIndex, letter ) =
    let
        index =
            toFloat intIndex

        positionAngle =
            Angle.turns <| index / 26 - 0.25

        rotationAngle =
            Angle.turns <| index / 26

        ( xPos, yPos ) =
            ( xCenter + radius * cos (Angle.inRadians positionAngle), yCenter + radius * sin (Angle.inRadians positionAngle) )
    in
    text_
        [ textAnchor "middle"
        , alignmentBaseline "central"
        , x <| String.fromFloat xPos
        , y <| String.fromFloat yPos
        , rotationToTransformProperty rotationAngle ( xPos, yPos )
        , fill color
        ]
        [ text letter ]


rotationToTransformProperty : Angle -> ( Float, Float ) -> Svg.Styled.Attribute msg
rotationToTransformProperty angle ( xPos, yPos ) =
    transform <| "rotate(" ++ ([ Angle.inDegrees angle, xPos, yPos ] |> List.map String.fromFloat |> String.join ",") ++ ")"



-- Subscriptions ---------------------------------------------------------------


subscriptions : Model -> Sub Msg
subscriptions _ =
    onKeyPress keyDecoder


keyDecoder : Decode.Decoder Msg
keyDecoder =
    Decode.map toKey (Decode.field "key" Decode.string)


toKey : String -> Msg
toKey keyValue =
    case String.uncons keyValue of
        Just ( char, "" ) ->
            CharacterKey char

        _ ->
            ControlKey keyValue
